# Tezos Blockly
![Tezos-blockly image](packages/tezos-blockly/tezos-blockly.png)
A visual smart contract editor for Tezos protocol. Written by Boris Ettinger and Igor Gordon.

Runs at www.tezos-blockly.net

Part of the Tezos Coinlist Hackathon. Received second place in the DeFi category.

Translated Blockly blocks into [LIGO](https://www.ligolang.org) smart contract language. Also compiles into Michelson using Ligo compiler. Backend is a derivative of the [WebIDE](https://ide.ligolang.org) backend of LIGO.

To run locally, [install](https://ligolang.org/docs/intro/installation) ligo compiler and then do ```yarn``` and then ```PORT=BBBB yarn start``` and open ```localhost:BBBB```.

To run with docker:

```
docker build -t tezos-blockly .
```

```
docker run -e PORT=5000 -p 5000:5000 -it tezos-blockly
```
and go to localhost:5000

