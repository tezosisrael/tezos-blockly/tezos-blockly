'use strict';

import ls from 'local-storage';
import * as Blockly from 'blockly';

function _restoreBlocks(workspace) {
  const blockly_local = ls.get('blockly-local');
  if (blockly_local) {
    const xml = Blockly.Xml.textToDom(blockly_local);
    Blockly.Xml.domToWorkspace(xml, workspace);
  }
}

function setupStorage(workspace) {
  window.setTimeout(function() {
    _restoreBlocks(workspace);
  }, 0);
  window.addEventListener('unload',
      function() {
        const xml = Blockly.Xml.workspaceToDom(workspace);
        ls.set('blockly-local', Blockly.Xml.domToText(xml));
      }, false);
}

export default setupStorage;
