'use strict';

import * as Blockly from 'blockly';
import { TypedVariableModal } from '@blockly/plugin-typed-variable-modal';

const createFlyout = function(workspace) {
    let xmlList = [];
    // Add your button and give it a callback name.
    const button = document.createElement('button');
    button.setAttribute('text', 'Create Typed Variable');
    button.setAttribute('callbackKey', 'callbackName');

    xmlList.push(button);

    // This gets all the variables that the user creates and adds them to the
    // flyout.
    const blockList = Blockly.VariablesDynamic.flyoutCategoryBlocks(workspace);
    xmlList = xmlList.concat(blockList);
    return xmlList;
};

function setupTypedVariables(workspace) {

    Blockly.Blocks['variables_set_dynamic'] = false;
    workspace.registerToolboxCategoryCallback('CREATE_TYPED_VARIABLE', createFlyout);
    const typedVarModal = new TypedVariableModal(workspace, 'callbackName', [["nat", "nat"], 
                                                                             ["int", "int"],
                                                                             ["tez", "tez"],
                                                                             ["string", "string"],                                                                             
                                                                             ["bool", "bool"]]);
    typedVarModal.init();
}

export default setupTypedVariables;


